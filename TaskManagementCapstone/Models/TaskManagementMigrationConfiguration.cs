﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using WebMatrix.WebData;

namespace TaskManagementCapstone.Models
{
    public class TaskManagementMigrationConfiguration : DbMigrationsConfiguration<TaskManagementCapstoneContext>
    {
        public TaskManagementMigrationConfiguration()
        {
        }

        protected override void Seed(TaskManagementCapstoneContext context)
        {
            base.Seed(context);

            AddStatuses(context);
            AddLevels(context);
            AddUser(context);
        }

        private void AddUser(TaskManagementCapstoneContext context)
        {
#if DEBUG   
            if (!WebMatrix.WebData.WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "Id", "Email", autoCreateTables: true);
            }
            string email = "a@b.com";
            if (!WebSecurity.UserExists(email))
            {
                string password = "pepperzoid34";
                Level level = context.Levels.Where(l => l.Value == 0).FirstOrDefault();
                Level nextLevel = context.Levels.Where(l => l.Value == 1).FirstOrDefault();
                WebSecurity.CreateUserAndAccount(email, password, new { Level_Id =  level.Id, NextLevel_Id = nextLevel.Id, Gold = 0 });
            }

#endif
        }

        /*
         * Add necessary Status objects
         */ 
        private void AddStatuses(TaskManagementCapstoneContext context)
        {
            if (context.Statuses.Count() == 0)
            {
                context.Statuses.Add(new Status() { Name = StatusType.Pending.ToString() });
                context.Statuses.Add(new Status() { Name = StatusType.Resolved.ToString() });
                context.Statuses.Add(new Status() { Name = StatusType.PastEndDate.ToString() });
                context.SaveChanges();
            }
        }

        /*
         * Add necessary Level objects
         */ 
        private void AddLevels(TaskManagementCapstoneContext context)
        {
            context.Levels.Add(new Level() { Value = 0, Experience = 0 });

            if (context.Levels.Count() == 0)
            {
                for(int i = 1; i <= 26; i++)
                {
                    context.Levels.Add(new Level() 
                    { 
                        Value = i,
                        Experience = (int)(Math.Pow(1.35, i) * 60) 
                    });
                }
                context.SaveChanges();
            }
        }


    }
}
