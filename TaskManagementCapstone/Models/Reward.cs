﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class Reward
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}