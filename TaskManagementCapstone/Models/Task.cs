﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class Task
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int Experience { get; set; }
        public string Name { get; set; }
        public Status Status { get; set; }
        public bool FullyComplete { get; set; }
        public int ParentId { get; set; }
        public int Gold { get { return 0; } }

        public int UserId { get; set; }
    }
}