﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagementCapstone.Models
{
    public interface ITaskManagementRepository
    {
        IQueryable<Task> GetTasksByUser(int userId);
        Status GetStatus(int id);
        Level GetLevel(int value);
        User GetUser(int id);
        User GetUserFromName(string name);
        User GetFullUserFromName(string name);

        bool Save();

        bool AddTask(Task task);
        bool UpdateTask(Task task);
        bool DeleteTask(Task task);
        bool UpdateUser(User user);
        bool AddReward(Reward reward);
    }
}
