﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class TaskManagementCapstoneContext : DbContext
    {
        public TaskManagementCapstoneContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;

            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<TaskManagementCapstoneContext, TaskManagementMigrationConfiguration>());
        }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<Reward> Rewards { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Level> Levels { get; set; }

        public DbSet<User> Users { get; set; }
    }
}