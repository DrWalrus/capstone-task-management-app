﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class TaskManagementRepository : ITaskManagementRepository
    {
        private TaskManagementCapstoneContext context;

        public TaskManagementRepository(TaskManagementCapstoneContext context)
        {
            this.context = context;
        }

        public IQueryable<Task> GetTasksByUser(int userId)
        {
            return context.Tasks.Where(t => t.Id == userId);
        }

        public Status GetStatus(int id)
        {
            return context.Statuses.Where(s => s.Id == id).First();
        }

        public Level GetLevel(int value)
        {
            return context.Levels.Where(l => l.Value == value).First();
        }


        public bool Save()
        {
            try
            {
                return context.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddTask(Task task)
        {
            try
            {
                context.Tasks.Add(task);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateTask(Task task)
        {
            try
            {
                context.Entry(task).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }           
        }

        public bool DeleteTask(Task task)
        {
            try
            {
                context.Tasks.Remove(task);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public IQueryable<Task> GetTasksByUser()
        {
            throw new NotImplementedException();
        }

        public User GetUser(int id)
        {
            return context.Users.Include("Level").Include("NextLevel").Include("Tasks").Where(u => u.Id == id).First();
        }


        public User GetUserFromName(string name)
        {
            return context.Users.Include("Level").Include("NextLevel").Include("Tasks").Where(u => u.Email == name).First();
        }

        public User GetFullUserFromName(string name)
        {
            return context.Users.Include("Level").Include("NextLevel").Include("Tasks").Where(u => u.Email == name).First();
        }

        public bool UpdateUser(User user)
        {
            try
            {
                context.Entry(user).State = EntityState.Modified;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }  
        }


        public bool AddReward(Reward reward)
        {
            try
            {
                context.Rewards.Add(reward);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}