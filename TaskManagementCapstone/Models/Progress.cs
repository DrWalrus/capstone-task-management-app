﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class Progress
    {
        public int UserId { get; set; }
        public Level Level { get; set; }
        public Level NextLevel { get; set; }
        public int Gold { get; set; }
        public int Experience { get; set; }
        public int PercentCompelete 
        { 
            get 
            { 
                return (Experience / NextLevel.Experience) * 100; 
            } 
        }
    }
}