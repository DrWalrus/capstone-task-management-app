﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public enum StatusType
    {
        Pending = 1,
        Resolved = 2,
        PastEndDate = 3
    }
}