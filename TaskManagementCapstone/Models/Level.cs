﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class Level
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public int Experience { get; set; }
    }
}