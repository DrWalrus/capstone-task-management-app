namespace TaskManagementCapstone.Models
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitializeDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Experience = c.Int(nullable: false),
                        Name = c.String(),
                        FullyComplete = c.Boolean(nullable: false),
                        ParentId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Status_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Status", t => t.Status_Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.Status_Id)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Gold = c.Int(nullable: false),
                        Level_Id = c.Int(),
                        NextLevel_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Levels", t => t.Level_Id)
                .ForeignKey("dbo.Levels", t => t.NextLevel_Id)
                .Index(t => t.Level_Id)
                .Index(t => t.NextLevel_Id);
            
            CreateTable(
                "dbo.Levels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Experience = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rewards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Rewards", new[] { "UserId" });
            DropIndex("dbo.User", new[] { "NextLevel_Id" });
            DropIndex("dbo.User", new[] { "Level_Id" });
            DropIndex("dbo.Tasks", new[] { "UserId" });
            DropIndex("dbo.Tasks", new[] { "Status_Id" });
            DropForeignKey("dbo.Rewards", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "NextLevel_Id", "dbo.Levels");
            DropForeignKey("dbo.User", "Level_Id", "dbo.Levels");
            DropForeignKey("dbo.Tasks", "UserId", "dbo.User");
            DropForeignKey("dbo.Tasks", "Status_Id", "dbo.Status");
            DropTable("dbo.Rewards");
            DropTable("dbo.Levels");
            DropTable("dbo.User");
            DropTable("dbo.Status");
            DropTable("dbo.Tasks");
        }
    }
}
