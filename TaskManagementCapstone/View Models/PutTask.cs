﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManagementCapstone.Models
{
    public class PutTask
    {
        public Task Task { get; set; }
        public bool Resolving { get; set; }
        public DateTime NewStart { get; set; }
        public DateTime NewEnd { get; set; }
    }
}