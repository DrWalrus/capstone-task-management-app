﻿var module = angular.module("TaskManagementApp", ["ui.bootstrap"]);

module.config(function ($routeProvider) {

    $routeProvider
        .when('/Calendar', {
            templateUrl: 'Templates/Calendar.html',
            controller: 'CalendarCtrl'
        })
        .when('/Statistics', {
            templateUrl: 'Templates/Statistics.html',
            controller: 'StatisticsCtrl'
        })
        .otherwise({ redirectTo: "/Calendar" });
});