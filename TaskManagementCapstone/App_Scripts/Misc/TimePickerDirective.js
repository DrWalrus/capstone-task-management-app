﻿/*
    Timepicker Directive
*/
module.directive("timePicker", function () {
    return {
        restrict: "E",
        scope: { time: '=' },
        controller: TimePickerCtrl,
        templateUrl: "/Templates/Misc/TimePicker.html",
        replace: true
    };
});