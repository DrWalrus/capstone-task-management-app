﻿function TimePickerCtrl($scope, $filter) {
    
    $scope.times = [];
    $scope.selected = new Date();

    
    //Iterate through hours in day
    for (var i = 0; i < 23; i++) {

        //Interate in increments of 15
        for (var j = 0; j < 60; j = j + 15) {
            var newDate = new Date();
            //Add a new date at for each hour, every 15 minutes
            newDate.setHours(i, j);
            $scope.times.push(
                {
                    value: newDate,
                    text: $filter('date')(newDate, 'shortTime')
                });
        }
    }

    $scope.update = function (time) {
       time.setHours($scope.selected.getHours(), $scope.selected.getMinutes());
    }

}