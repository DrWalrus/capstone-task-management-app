﻿module.factory("dataService", function ($http, $q) {

    var _user = {};

    var _userLevelInfo = {};

    var _getUser = function (id) {
        if (_user != null) {
            var deferred = $q.defer();

            $http.get("api/user?full=true")
                .then(function (result) {
                    //Success
                    angular.copy(result.data, _user);
                    _user.percentComplete = (_user.experience / 110) * 100
                    deferred.resolve();
                },
                function () {
                    //Error
                    deferred.reject();
                });

            /*_user.tasks = [
                {
                    name: 'get task directive working',
                    start: new Date(2013, 9, 27, 15, 0),
                    end: new Date(2013, 9, 27, 17, 0),
                    status: {
                        id: 1,
                        name: 'Pending'
                    },
                    fullyComplete: true
                },
                {
                    name: 'notifications',
                    start: new Date(2013, 9, 20, 17, 0),
                    end: new Date(2013, 9, 20, 18, 0),
                    status: {
                        id: 1,
                        name: 'Pending'
                    },
                    fullyComplete: true
                },
                {
                    name: 'get task directive working',
                    start: new Date(2013, 9, 20, 15, 0),
                    end: new Date(2013, 9, 20, 17, 0),
                    status: {
                        id: 1,
                        name: 'Pending'
                    },
                    fullyComplete: true
                },
                {
                     name: 'New Event Directive',
                     start: new Date(2013, 9, 19, 15, 0),
                     end: new Date(2013, 9, 19, 17, 0),
                     status: {
                         id: 2,
                         name: 'PastEndDate'
                     },
                     fullyComplete: true
                },
                {
                    name: 'Organize stuff',
                    start: new Date(2013, 10, 18, 15, 0),
                    end: new Date(2013, 10, 18, 17, 0),
                    status: {
                        id: 3,
                        name: 'Resolved'
                    },
                    fullyComplete: true,
                    experience: 120,
                    gold: 120 / 5
                },
            ];*/
        }
    };

    var _getUserLevelInfo = function () {

        var deferred = $q.defer();

        $http.get("api/user")
            .then(function (result) {
                //Success
                angular.copy(result.data.level, _user.level);
                angular.copy(result.data.experience, _user.experience);
                //_user.percentComplete = (_user.experience / 110) * 100
                deferred.resolve();
            },
            function () {
                //Error
                deferred.reject();
            });

    };

    var _addTask = function (newTask) {
        
        var deferred = $q.defer();

        $http.post("api/tasks", newTask)
            .then(function (result) {
                //Success
                _user.tasks.unshift(angular.copy(result.data));
                deferred.resolve();
            },
            function () {
                //Error
                deferred.reject();
            });
        return deferred.promise;
    };

    var _resolveTask = function (newTask, start, end) {

        var deferred = $q.defer();

        var resolveTask = {
            task: newTask,
            resolving: true,
            newStart: start,
            newEnd: end
        };
        $http.put("api/tasks", resolveTask)
           .then(function (result) {
               //Success
               var index = _user.tasks.indexOf(task);
               _user.tasks.splice(index, 1);
               for (var i in result.data) {
                   _user.tasks.unshift(angular.copy(result.data[i]));
               }
               _getUserLevelInfo();
               deferred.resolve();
           },
           function () {
               //Error
               deferred.reject();
           });

        return deferred.promise;
        
    }

    var _updateTask = function (_task) {

        var deferred = $q.defer();

        var resolveTask = {
            task: _task,
            resolving: false
        };

        $http.put("api/tasks", resolveTask)
           .then(function (result) {
               //Success
               var index = _user.tasks.indexOf(_task);
               _user.tasks.splice(index, 1);
               for(var i in result.data){
                   _user.tasks.unshift(angular.copy(result.data[i]));
               }
               deferred.resolve();
           },
           function () {
               //Error
               deferred.reject();
           });

        return deferred.promise;

    }

    var _deleteTask = function (task) {

        var deferred = $q.defer();

        $http.delete("api/tasks", task)
           .then(function (result) {
               //Success
               var index = _user.tasks.indexOf(task);
               _user.tasks.splice(task, 1);
               deferred.resolve();
           },
           function () {
               //Error
               deferred.reject();
           });

        return deferred.promise;
    }

    var _addReward = function (reward) {

        var deferred = $q.defer();

        $http.post("api/rewards", reward)
           .then(function (result) {
               //Success
               _user.rewards.unshift(angular.copy(result.data));
               deferred.resolve();
           },
           function () {
               //Error
               deferred.reject();
           });

        return deferred.promise;
    }

    return {
        user: _user,
        userLevelInfo: _userLevelInfo,
        getUser: _getUser,
        getUserLevelInfo: _getUserLevelInfo,
        addTask: _addTask,
        resolveTask: _resolveTask,
        updateTask: _updateTask,
        deleteTask: _deleteTask,
        addReward: _addReward
    };

});