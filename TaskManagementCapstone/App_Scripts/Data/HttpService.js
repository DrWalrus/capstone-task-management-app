﻿module.factory('httpService', function ($http, $q) {

    var _get = function (url) {

        var deferred = $q.defer();

        $http.get(url)
            .then(function (result) {
                //Success
                deferred.resolve(result.data);
            },
            function () {
                //Error
                deferred.reject();
            });

        return deferred.promise;
    }

    var _post = function (url, data) {

        var deferred = $q.defer();

        $http.post(url, data)
            .then(function (result) {
                //Success
                deferred.resolve(result.data);
            },
            function () {
                //Error
                deferred.reject();
            });

        return deferred.promise;
    }

    var _put = function (url, data) {

        var deferred = $q.defer();

        $http.put(url, data)
            .then(function (result) {
                //Success
                deferred.resolve(result.data);
            },
            function () {
                //Error
                deferred.reject();
            });

        return deferred.promise;
    }

    var _delete = function (url, data) {

        var deferred = $q.defer();

        $http.delete(url, data)
            .then(function (result) {
                //Success
                deferred.resolve(result.data);
            },
            function () {
                //Error
                deferred.reject();
            });

        return deferred.promise;
    }

    return {
        get: _get,
        post: _post,
        put: _put,
        del: _delete 
    }

});