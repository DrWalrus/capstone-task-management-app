﻿module.factory("notificationService", function () {

    var _maximumNotifications = 3;
    var _notifications = [];

    var _add = function (_message, _type) {
        _notifications.unshift({
            message: _message,
            type: _type
        });

        if (_notifications.length >= _maximumNotifications) {
            _notifications.splice(_maximumNotifications, _notifications.length - _maximumNotifications);
        }
    }

    var _clear = function () {
        _notifications = [];
    }

    var _remove = function (index) {
        _notifications.splice(index, 1);
    }
    

    return {
        notifications: _notifications,
        add: _add,
        clear: _clear,
        remove: _remove
    };

});