﻿/*
    Ntofication Directive
*/
module.directive("notifications", function () {
    return {
        restrict: "E",
        scope: { time: '=' },
        controller: NotificationCtrl,
        templateUrl: "/Templates/Misc/Notifications.html",
        replace: true
    };
});