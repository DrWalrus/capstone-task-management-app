﻿/*var module = angular.module("TaskManagementApp", ["ui.bootstrap"]);

module.config(function ($routeProvider) {

    $routeProvider.otherwise({ redirectTo: "/" });
});

module.directive("tasks", function() {
    return {
        restrict: "E",
        controller: TasksCtrl,
        templateUrl: "/Templates/TaskList.html",
        replace: true
    };
});

module.directive("userLevelInfo", function () {
    return {
        restrict: "E",
        controller: UserCtrl,
        templateUrl: "/Templates/UserLevelInfo.html",
        replace: true
    };
});

module.directive('task', function () {
    return {
        require: '^tasks',
        restrict: 'E',
        controller: TaskCtrl,
        scope: { task: "=" },
        templateUrl: "/Templates/Task/Task.html"
    };
})

module.factory("dataService", function ($http, $q) {

    var _user = {};

    var _userLevelInfo = {};

    var _tasks = [];

    var _getUser = function (id) {
        var deferred = $q.defer();

        $http.get("api/user?full=true")
            .then(function (result) {
                //Success
                angular.copy(result.data, _user);
                _user.percentComplete = (_user.experience / 110) * 100
                deferred.resolve();
            },
            function () {
                //Error
                deferred.reject();
            });

        
    };

    var _getUserLevelInfo = function () {

        var deferred = $q.defer();

        $http.get("api/user")
            .then(function (result) {
                //Success
                angular.copy(result.data.level, _user.level);
                angular.copy(result.data.experience, _user.experience);
                _user.percentComplete = (_user.experience / 110) * 100
                deferred.resolve();
            },
            function () {
                //Error
                deferred.reject();
            });
    };
 
    var _addTask = function (newTask) {
        var result = angular.copy(newTask, result);
        _user.tasks.push(result);
    };

    var _resolveTask = function (task) {
        
        task.status.name = "Resolved";
        _getUserLevelInfo();
    }

    var _updateTask = function (task) {
        return task;
    }

    var _deleteTask = function (task) {
        var index = _user.tasks.indexOf(task);
        if (index > -1) {
            _user.tasks.splice(index, 1);
        }
    }

    return {
        user: _user,
        userLevelInfo: _userLevelInfo,
        tasks: _tasks,
        getUser: _getUser,
        getUserLevelInfo: _getUserLevelInfo,
        addTask: _addTask,
        resolveTask: _resolveTask,
        updateTask: _updateTask,
        deleteTask: _deleteTask
    };

});

function TasksCtrl($scope, dataService) {

    $scope.data = dataService;
    $scope.displayTaskForm = false;
    $scope.newTask;

    $scope.showForm = function () {
        $scope.displayTaskForm = true;
    }

    $scope.saveTask = function (taskForm) {
        if (taskForm.$valid) {
            $scope.newTask.start = new Date($scope.newTask.start);
            $scope.newTask.end = new Date($scope.newTask.end);
            dataService.addTask($scope.newTask);
            $scope.newTask = {};
            $scope.displayTaskForm = false;
        }
    }

    $scope.cancelSaveTask = function() {
        $scope.newTask = {};
        $scope.displayTaskForm = false;
    }
}

function TaskCtrl($scope, dataService) {

    //View states
    $scope.states = {
        display: "display",
        deleting: "deleting",
        edit: "edit",
        resolving: "resolving"
    }

    //Current View State
    $scope.state = $scope.states.display;

    //Contains task that is currently being edited or resolved
    var bufferTask;

    $scope.updateTask = function (task) {
        dataService.updateTask(task);
        $scope.state = $scope.states.display;
    }

    $scope.showEdit = function (task) {
        bufferTask = angular.copy(task);
        $scope.state = $scope.states.edit;
    }

    $scope.cancel = function (task) {
        
        if ($scope.state == $scope.states.edit || $scope.state == $scope.states.resolving) {
            task = angular.copy(bufferTask);
        }

        $scope.state = $scope.states.display;
    }

    $scope.showDelete = function (task) {
        bufferTask = angular.copy(task);
        $scope.state = $scope.states.deleting;
    }

    $scope.deleteTask = function (task) {
        dataService.deleteTask(task);
    }

    $scope.showResolve = function (task) {
        bufferTask = angular.copy(task);
        $scope.state = $scope.states.resolving;
    }

    $scope.resolveTask = function (task) {
        dataService.resolveTask(task);
        $scope.state = $scope.states.display;
    }
}

function UserCtrl($scope, dataService) {
    //dataService.getUserLevelInfo();
    dataService.getUser();
    $scope.data = dataService;

    $scope.getProgressBarStyle = function(){
        return { width: dataService.user.percentComplete + "%" };
    }
}*/