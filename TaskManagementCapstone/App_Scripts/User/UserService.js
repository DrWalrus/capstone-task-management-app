﻿module.factory('userService', function (httpService, notificationsService) {
    var USER_PATH = '/api/users';
    

    var _user;

    httpService.get(httpService)
        .then(function (user) {
            _user = user;
        }, function () {
            notificationsService.add('Unable to get user', 'error');
        });

    return {
        tasks: _user.tasks,
        progress: _user.progress
    }
});