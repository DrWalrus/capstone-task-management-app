﻿function UserCtrl($scope, dataService) {
    dataService.getUser();
    $scope.data = dataService;

    $scope.getProgressBarStyle = function () {
        return { width: dataService.user.percentComplete + "%" };
    }
}