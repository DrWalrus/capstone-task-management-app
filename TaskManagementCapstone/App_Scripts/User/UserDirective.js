﻿module.directive("userLevelInfo", function () {
    return {
        restrict: "E",
        controller: UserCtrl,
        templateUrl: "/Templates/UserLevelInfo.html",
        replace: true
    };
});