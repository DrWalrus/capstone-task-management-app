﻿function TaskCtrl($scope, dataService, notificationService) {

    //View states
    $scope.states = {
        display: "display",
        deleting: "deleting",
        edit: "edit",
        resolving: "resolving"
    }

    //Current View State
    $scope.state = $scope.states.display;

    //Contains task that is currently being edited or resolved
    var bufferTask;

    $scope.updateTask = function (task) {
        dataService.updateTask(task)
            .then(function () {
                $scope.state = $scope.states.display;
            }, function () {
                notificationService.add("Unable to edit task", "error");
                $scope.cancel(task);
            });   
    }

    $scope.showEdit = function (task) {
        bufferTask = angular.copy(task);
        $scope.state = $scope.states.edit;
    }

    $scope.cancel = function (task) {

        if ($scope.state == $scope.states.edit || $scope.state == $scope.states.resolving) {
            task = angular.copy(bufferTask);
        }

        $scope.state = $scope.states.display;
    }

    $scope.showDelete = function (task) {
        bufferTask = angular.copy(task);
        $scope.state = $scope.states.deleting;
    }

    $scope.deleteTask = function (task) {
        dataService.deleteTask(task)
            .then(function () {
            }, function () {
                notificationService.add("Unable to delete task", "error");
                $scope.cancel(task);
            });
    }

    $scope.showResolve = function (task) {
        bufferTask = angular.copy(task);
        $scope.state = $scope.states.resolving;
    }

    $scope.resolveTask = function (task) {
        dataService.resolveTask(task)
            .then(function () {
            }, function () {
                notificationService.add("Unable to resolve task", "error");
                $scope.cancel(task);
            });
        $scope.state = $scope.states.display;
    }
}