﻿module.directive('taskEdit', function () {
    return {
        require: '^task',
        restrict: 'E',
        templateUrl: "/Templates/Task/Edit.html"
    };
})