﻿module.directive('taskResolved', function () {
    return {
        require: '^task',
        restrict: 'E',
        templateUrl: "/Templates/Task/Resolved.html"
    };
})