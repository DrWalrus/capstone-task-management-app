﻿module.directive('taskPending', function () {
    return {
        require: '^task',
        restrict: 'E',
        templateUrl: "/Templates/Task/Pending.html"
    };
})