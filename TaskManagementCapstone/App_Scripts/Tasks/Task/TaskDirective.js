﻿module.directive('task', function () {
    return {
        require: '^tasks',
        restrict: 'E',
        controller: TaskCtrl,
        scope: { task: "=" },
        templateUrl: "/Templates/Task/Task.html"
    };
})