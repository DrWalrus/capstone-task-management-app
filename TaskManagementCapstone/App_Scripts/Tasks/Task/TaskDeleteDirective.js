﻿module.directive('taskDelete', function () {
    return {
        require: '^task',
        restrict: 'E',
        templateUrl: "/Templates/Task/Delete.html"
    };
})