﻿module.directive('taskResolving', function () {
    return {
        require: '^task',
        restrict: 'E',
        templateUrl: "/Templates/Task/Resolving.html"
    };
})