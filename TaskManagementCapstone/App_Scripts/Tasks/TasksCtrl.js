﻿function TasksCtrl($scope, dataService, notificationService) {

    $scope.data = dataService;
    $scope.notesService = notificationService;
    $scope.newTask = {
        name: '',
        start: new Date(),
        end: new Date()
        };
    $scope.prompt = "Create a new task";

    $scope.save = function (taskForm) {
        dataService.addTask($scope.newTask)
            .then(function() {
            }, function(){
                $scope.notesService.add("Unable to create new task", "error");
            });
    }
}