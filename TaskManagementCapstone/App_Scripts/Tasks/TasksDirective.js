﻿/*
    Tasks Directive
*/
module.directive("tasks", function () {
    return {
        restrict: "E",
        controller: TasksCtrl,
        templateUrl: "/Templates/Tasks.html",
        replace: true,
        scope: { filterExpression: '&' }
    };
});