﻿module.directive("rewards", function () {
    return {
        restrict: "E",
        controller: RewardsCtrl,
        templateUrl: "/Templates/RewardList.html",
        replace: true,
        scope: { filterExpression: '&' }
    };
});