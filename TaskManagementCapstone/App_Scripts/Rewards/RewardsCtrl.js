﻿function RewardsCtrl($scope, dataService, notificationService) {

    $scope.data = dataService;
    $scope.isCollapsed = true;
    $scope.newReward;
    $scope.prompt = "Create a new reward";

    $scope.save = function () {
        dataService.addReward($scope.newReward)
            .then(function () {
            }, function () {
                notificationService.add("Unable to create new reward", "error");
            });
    }

}