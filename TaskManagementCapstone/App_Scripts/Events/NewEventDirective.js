﻿module.directive('newEvent', function () {
    return {
        restrict: 'E',
        controller: NewEventCtrl,
        scope: {
            event: "=",
            onSave: '&',
            promptText: "=" },
        templateUrl: "/Templates/Event/NewEvent.html"
    };
})