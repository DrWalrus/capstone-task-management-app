﻿function NewEventCtrl($scope, dataService) {

    $scope.data = dataService;
    $scope.eventFormCollapsed = true;
    /*$scope.newEvent = {
        name: '',
        start: new Date(),
        end: new Date()
    };*/

    $scope.overflowStyle = {};

    $scope.save = function (eventForm, newEvent) {
        if (eventForm.$valid) {
            $scope.onSave();
            $scope.event = {};
            $scope.eventFormCollapsed = true;          
        }
    }

    $scope.cancel = function () {
        $scope.newEvent = {};
        $scope.closeForm();
    }

    $scope.openForm = function () {
        $scope.eventFormCollapsed = false;
        //$scope.enableoverflowStyle();
    };

    $scope.closeForm = function () {
        $scope.eventFormCollapsed = true;
        $scope.disableoverflowStyle();
    };

    $scope.enableoverflowStyle = function () {
        $scope.overflowStyle = { overflow: 'visible' };
    };

    $scope.disableoverflowStyle = function () {
        $scope.overflowStyle = {};
    };

}