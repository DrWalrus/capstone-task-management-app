﻿function CalendarCtrl($scope, dataService) {

    $scope.currentDate = moment();

    $scope.states = {
        day: {
            duration: 1,
            unit: 'day',
            display: function () { return $scope.currentDate.format('D MMM YYYY'); }
        },
        week: {
            duration: 1,
            unit: 'week',
            display: function () { return $scope.currentDate.startOf('week').format('MMM D') + ' - ' + $scope.currentDate.endOf('week').format('MMM D'); }
        },
        month: {
            duration: 1,
            unit: 'month',
            display: function () { return $scope.currentDate.format('MMMM YYYY'); }
        },
    };

    $scope.currentState = $scope.states.day;

    $scope.getDisplayDate = function () {
        return $scope.currentState.display;
    }

    $scope.increment = function () {
        $scope.currentDate.add($scope.currentState.unit, $scope.currentState.duration);
    }

    $scope.decrement = function () {
        $scope.currentDate.subtract($scope.currentState.unit, $scope.currentState.duration);
    }

    $scope.setState = function (state) {
        $scope.currentState = state;
    }

    $scope.today = function () {
        $scope.currentDate = moment();
        $scope.currentState = $scope.states.day;
    }

    $scope.filterByDate = function () {
        return function (task) {
            var start = $scope.currentDate.clone().startOf($scope.currentState.unit);
            var end = $scope.currentDate.clone().endOf($scope.currentState.unit);
            return (start.isBefore(task.start) && end.isAfter(task.start));
        };
    };

}