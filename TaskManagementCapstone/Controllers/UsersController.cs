﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagementCapstone.Models;

namespace TaskManagementCapstone.Controllers
{
    public class UsersController : ApiController
    {
        private ITaskManagementRepository repository;

        public UsersController(ITaskManagementRepository repository)
        {
            this.repository = repository;
        }

        public User Get(bool full = false)
        {
            User user;
            if (full)
            {
                user = repository.GetFullUserFromName(User.Identity.Name);
            }
            else
            {
                user = repository.GetUserFromName(User.Identity.Name);
            }
            //Check if user has reached the next level
            if (user.Experience >= user.NextLevel.Experience)
            {
                user.Level = user.NextLevel;
                if (user.NextLevel.Value < 26)
                {
                    user.NextLevel = repository.GetLevel(user.NextLevel.Value + 1);

                    if (repository.UpdateUser(user) && repository.Save())
                    {
                        user = repository.GetFullUserFromName(User.Identity.Name);
                    }
                } 
            }
            return user;
        }

        private User GetFullUser()
        {
            var user = new User()
            {
                Email = "bill",
                Level = new Level(),
                NextLevel = new Level(),
            };
            return user;
        }

        public HttpResponseMessage Put([FromBody]User user)
        {
            if (repository.UpdateUser(user) && repository.Save())
            {
                return Request.CreateResponse(HttpStatusCode.Created, user);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
