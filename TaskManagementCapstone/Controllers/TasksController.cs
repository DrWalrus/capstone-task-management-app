﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagementCapstone.Models;

namespace TaskManagementCapstone.Controllers
{
    public class TasksController : ApiController
    {
        private ITaskManagementRepository repository;

        public TasksController(ITaskManagementRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<Task> Get(int userId)
        {
            return repository.GetTasksByUser(userId).OrderByDescending(t => t.Start);
        }

        public HttpResponseMessage Post([FromBody]Task task)
        {
            task.Status = repository.GetStatus((int)StatusType.Pending);
            task.UserId = repository.GetFullUserFromName(User.Identity.Name).Id;

            if (repository.AddTask(task) && repository.Save())
            {
                return Request.CreateResponse(HttpStatusCode.Created, task);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put([FromBody]PutTask putTask)
        {
            bool resolved = true;
            var returnTasks = new List<Task>();
            Task newTask;

            if (putTask.Resolving)
            {
                putTask.Task.Status = repository.GetStatus((int)StatusType.Resolved);
                if (!putTask.Task.FullyComplete)
                {
                    resolved = Resolve(putTask.Task, putTask.NewStart, putTask.NewEnd, out newTask);
                    returnTasks.Add(newTask);
                }
                //var user = repository.GetFullUserFromName(User.Identity.Name);
                //user.Gold += putTask.Task.Gold;
            }

            if (resolved && repository.UpdateTask(putTask.Task) && repository.Save())
            {
                returnTasks.Add(putTask.Task);
                return Request.CreateResponse(HttpStatusCode.OK, returnTasks);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }


        private bool Resolve(Task task, DateTime newStart, DateTime newEnd, out Task newTask)
        {
            newTask = new Task()
            {
                Name = task.Name,
                ParentId = task.Id,
                Status = repository.GetStatus((int)StatusType.Pending),
                Start = newStart,
                End = newEnd
            };

            bool result = repository.AddTask(newTask);
            return result;
        }
    }
}
