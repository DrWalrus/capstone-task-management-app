﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaskManagementCapstone.Models;

namespace TaskManagementCapstone.Controllers
{
    public class RewardsController : ApiController
    {
        private ITaskManagementRepository repository;

        public RewardsController(ITaskManagementRepository repository)
        {
            this.repository = repository;
        }

        public HttpResponseMessage Post([FromBody]Reward reward)
        {
            if (repository.AddReward(reward) && repository.Save())
            {
                return Request.CreateResponse(HttpStatusCode.Created, reward);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
