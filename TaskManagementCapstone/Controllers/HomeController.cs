﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskManagementCapstone.Models;

namespace TaskManagementCapstone.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ITaskManagementRepository repository;

        public HomeController(ITaskManagementRepository repository)
        {
            this.repository = repository;
        }
        public ActionResult Index()
        {
            
            return View();
        }
    }
}
